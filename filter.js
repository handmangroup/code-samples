/**
 * @file
 * Uses the values of the selected option in the page banner title.
 */

(function($) {
  Drupal.behaviors.filteringEffect = {
    attach: function(context) {

      var $this = $(".view-blog-list.view-display-id-page_2");
      var selected = $this.find("form#views-exposed-form-blog-list-page-2 select");
      var selected_text = selected.find("option").filter(":selected").text();
      var banner = $this.find("div.banner");

      banner.html("<h1>Blogs</h1><p>Tagged with " + selected_text + "</p>");
      var banner_text = banner.find("p");

      if (selected.find("option").filter(":selected").text() == "TOPICS") {
        $(banner_text).hide();
      }
    }
  };
})(jQuery);
