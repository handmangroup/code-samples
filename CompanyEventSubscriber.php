<?php

namespace Drupal\login_redirect\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;


class CompanyEventSubscriber implements EventSubscriberInterface {

  public function checkUserUidRedirection(GetResponseEvent $event) {
    if (\Drupal::currentUser()->isAnonymous()) {
      return;
    }
    $request_uri = urldecode(\Drupal::request()->getRequestUri());
    if (preg_match('/\{company\}/', $request_uri)) {
      $current_user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
      $company = $current_user->get('field_company')->target_id;
      $request_uri = preg_replace('/\{company\}/', $company, $request_uri);
      $response = new RedirectResponse($request_uri, 301);
      $response->send();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkUserUidRedirection');
    return $events;
  }
}