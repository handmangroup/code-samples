<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<?php
$block = module_invoke('views', 'block_view', 'featured_event_calendar-block');
print render($block['content']);
?>
<div class="container">
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
        <?php print $title; ?>
    <?php endif; ?>
    <p>Test</p>
    <?php print render($title_suffix); ?>
    <div class="view-header">
        <?php print $header; ?>
        <?php if ($pager): ?>
            <?php print $pager; ?>
        <?php endif; ?>
    </div>

    <div class="view-filters">
        <?php $view = views_get_view('event_calendar');
        $display_id = 'page_1';
        $view->set_display($display_id);
        $view->init_handlers();
        $form_state = array(
          'view' => $view,
          'display' => $view->display_handler->display,
          'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
          'method' => 'get',
          'rerender' => TRUE,
          'no_redirect' => FALSE,
        );
        $form = drupal_build_form('views_exposed_form', $form_state);
        print drupal_render($form); ?>

    </div>

    <div class="layouts">
        <div class="calendar-view-active"><a href="/calendar"><span class="glyphicon glyphicon-th" aria-hidden="true"></span></a></div>
        <div class="list-view-na"><a href="/event-list"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span></a></div>
    </div>
    <?php if ($attachment_before): ?>
        <div class="attachment attachment-before">
            <?php print $attachment_before; ?>
        </div>
    <?php endif; ?>
</div>
<?php if ($rows): ?>
    <div class="view-content">
        <p>Test</p>
        <?php print $rows; ?>
    </div>
<?php elseif ($empty): ?>
    <div class="view-empty">
        <?php print $empty; ?>
    </div>
<?php endif; ?>



<?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
        <?php print $attachment_after; ?>
    </div>
<?php endif; ?>

<?php if ($more): ?>
    <?php print $more; ?>
<?php endif; ?>

<?php if ($footer): ?>
    <div class="view-footer">
        <?php print $footer; ?>
    </div>
<?php endif; ?>

<?php if ($feed_icon): ?>
    <div class="feed-icon">
        <?php print $feed_icon; ?>
    </div>
<?php endif; ?>

<?php /* class view */ ?>
